package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/domainr/whois"
)

type Response struct { // {{{
	Error  string `json:"error"`
	Result string `json:"result"`
} // }}}
func whoisQuery(data string) (string, error) { // {{{
	// Run whois on the user-specified object.
	response, err := whois.Fetch(data)
	if err != nil {
		return "", err
	}
	return string(response.Body), nil
} // }}}
func jsonResponse(w http.ResponseWriter, x interface{}) { // {{{
	// JSON-encode x.
	bytes, err := json.Marshal(x)
	if err != nil {
		panic(err)
	}
	// Write the encoded data to the ResponseWriter.
	// This will send the response to the client.
	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
} // }}}
func main() {
	fileServer := http.FileServer(http.Dir("static/"))

	http.HandleFunc("/", // {{{
		func(w http.ResponseWriter, r *http.Request) {
			fileServer.ServeHTTP(w, r)
		},
	) // }}}
	http.HandleFunc("/whois", func(w http.ResponseWriter, r *http.Request) { // {{{
		if r.Method != "POST" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		data := r.PostFormValue("data")
		result, err := whoisQuery(data)
		if err != nil {
			jsonResponse(w, Response{Error: err.Error()})
			return
		}
		jsonResponse(w, Response{Result: result})
	}) // }}}

	log.Fatal(http.ListenAndServe("localhost:8080", nil))
}
